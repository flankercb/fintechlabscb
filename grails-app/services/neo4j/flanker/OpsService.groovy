package neo4j.flanker

import grails.gorm.services.Service

//tag::service[]
import groovy.transform.CompileStatic
import neo4j.movies.Movie

@SuppressWarnings(['UnusedVariable', 'SpaceAfterOpeningBrace', 'SpaceBeforeClosingBrace'])
@CompileStatic
@Service(Movie)
abstract class OpsService {


    List<Fighter> searchFighter(String q, int limit = 100) { // <1>
        List<Fighter> results
        if (q) {
            results = Fighter.where {
                name ==~ "%${q}%"  // <2>
            }.list(max: limit)
        } else {
            results = [] // <3>
        }
        results
    }











   /* @Cypher("""MATCH ${Movie m}<-[:ACTED_IN]-${Person p}
               RETURN ${m.title} as movie, collect(${p.name}) as cast
               LIMIT $limit""")
    protected abstract List<Map<String, Iterable<String>>> findMovieTitlesAndCast(int limit)
    //end::graph[]

    //tag::d3format[]
    @ReadOnly
    Map<String, Object> graph(int limit = 100) {
        toD3Format(findMovieTitlesAndCast(limit))
    }

    @SuppressWarnings('NestedForLoop')
    @CompileDynamic
    private static Map<String, Object> toD3Format(List<Map<String, Iterable<String>>> result) {
        List<Map<String, String>> nodes = []
        List<Map<String, Object>> rels = []
        int i = 0
        for (entry in result) {
            nodes << [title: entry.movie, label: 'movie']
            int target = i
            i++
            for (String name : (Iterable<String>) entry.cast) {
                def actor = [title: name, label: 'actor']
                int source = nodes.indexOf(actor)
                if (source == -1) {
                    nodes << actor
                    source = i++
                }
                rels << [source: source, target: target]
            }
        }
        [nodes: nodes, links: rels]
    }*/
    //end::d3format[]
}
