package neo4j.flanker

import groovy.transform.CompileStatic
import neo4j.movies.MovieService

//tag::controller[]
@CompileStatic
class OpsController {
    static responseFormats = ['json', 'xml']
//end::controller[]

    // tag::service[]
    OpsService opsService
    //end::service[]

    // tag::show[]
    def show(String title) {
        println "********************************** FIGHTER SHOW **********************************"

        respond opsService.searchFighter(title)

//        respond movieService.find(title)
    }
    //end::show[]

    // tag::search[]
    /* def search(String q) {
         respond opsService.search(q)
     }
     //end::search[]

     // tag::graph[]
     def graph() {
         respond opsService.graph(params.int('limit', 100))
     }*/
    //end::graph[]
}


