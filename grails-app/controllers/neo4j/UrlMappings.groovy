package neo4j

class UrlMappings {

    static mappings = {

        "/ops/$title"(controller: 'ops', action: 'show')
        "/ops/show/$title"(controller: 'ops', action: 'show')
        '500'(view: '/error')
        '404'(view: '/notFound')
    }
}
