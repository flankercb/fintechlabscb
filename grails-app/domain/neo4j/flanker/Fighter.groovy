package neo4j.flanker

import grails.compiler.GrailsCompileStatic


@GrailsCompileStatic
class Fighter {
    String name

    static constraints = {
    }
}
